import os
import argparse
import logging
import time

import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchsummary import summary
from torch.utils.data.sampler import SubsetRandomSampler

import numpy as np
from sklearn.metrics import balanced_accuracy_score

from cnn import ConfigurableNet
from datasets import KMNIST, K49


# Import Configspace-utilities
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

# Import SMAC-utilities
from smac.tae.execute_func import ExecuteTAFuncDict
from smac.scenario.scenario import Scenario
from smac.facade.smac_facade import SMAC

# Imports for analysis of important parameters
from cave.cavefacade import CAVE
import json
from scipy import stats


def eval(model, loader, device, train=False):
    """
    Evaluation method
    :param model: Model to evaluate
    :param loader: data loader for either training or testing set
    :param device: torch device
    :param train: boolean to indicate if training or test set is used
    :return: accuracy on the data
    """
    true, pred = [], []
    with torch.no_grad():  # no gradient needed
        for images, labels in loader:
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            true.extend(labels)
            pred.extend(predicted)
        # return balanced accuracy where each sample is weighted according to occurrence in dataset
        score = balanced_accuracy_score(true, pred)
        str_ = 'rain' if train else 'est'
        logging.info('T{0} Accuracy of the model on the {1} t{0} images: {2}%'.format(str_, len(true), score))
    return score


def train(train_data,
          test_data,
          model_config,
          seed=42,
          num_epochs=10,
          batch_size=50,
          learning_rate=0.001,
          channels = 3,
          layers = 2,
          pooling = True,
          batchnorm = True,
          activation = "tanh",
          kernel_size=3,
          dropout=0,
          train_criterion=torch.nn.CrossEntropyLoss,
          model_optimizer=torch.optim.Adam,
          data_augmentations=None,
          dimension_change=0,
          valid_sampler=None,
          train_sampler=None,
          ichannels=1,
          width=28,
          height=28,
          save_model_str=None):
    """
    Training loop for configurableNet.
    :param dataset: which dataset to load (str)
    :param model_config: configurableNet config (dict)
    :param num_epochs: (int)
    :param batch_size: (int)
    :param learning_rate: model optimizer learning rate (float)
    :param train_criterion: Which loss to use during training (torch.nn._Loss)
    :param model_optimizer: Which model optimizer to use during trainnig (torch.optim.Optimizer)
    :param data_augmentations: List of data augmentations to apply such as rescaling.
        (list[transformations], transforms.Composition[list[transformations]], None)
        If none only ToTensor is used
    :return:
    """
    train_criterion = train_criterion()  # not instantiated until now

    # Device configuration (fixed to cpu as we don't provide GPUs for the project)
    device = torch.device('cpu')  # 'cuda:0' if torch.cuda.is_available() else 'cpu')

    if data_augmentations is None:
        # We only use ToTensor here as that is al that is needed to make it work
        data_augmentations = transforms.ToTensor()
    elif isinstance(type(data_augmentations), list):
        data_augmentations = transforms.Compose(data_augmentations)
    elif not isinstance(data_augmentations, transforms.Compose):
        raise NotImplementedError


    train_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                               sampler=train_sampler)
    validation_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                                    sampler=valid_sampler)



    model = ConfigurableNet(model_config,
                            num_classes=dataset.n_classes,
                            height=dataset.img_rows,
                            width=dataset.img_cols,
                            channels=dataset.channels,
                            conv_channels=channels,
                            pooling=pooling,
                            dropout=dropout,
                            ks=kernel_size,
                            dimension_change=dimension_change,
                            batchnorm=batchnorm,
                            activation=activation,
                            ).to(device)
    total_model_params = np.sum(p.numel() for p in model.parameters())

    equal_freq = [1 / train_dataset.n_classes for _ in range(train_dataset.n_classes)]
    logging.debug('Train Dataset balanced: {}'.format(np.allclose(test_dataset.class_frequency, equal_freq)))
    logging.debug(' Test Dataset balanced: {}'.format(np.allclose(test_dataset.class_frequency, equal_freq)))
    logging.info('Generated Network:')
    summary(model, (train_dataset.channels, train_dataset.img_rows, train_dataset.img_cols), device='cpu')


    # Train the model
    optimizer = model_optimizer(model.parameters(), lr=learning_rate)
    total_step = len(train_loader)
    train_time = time.time()
    epoch_times = []
    for epoch in range(num_epochs):
        logging.info('#' * 120)
        epoch_start_time = time.time()
        for i_batch, (images, labels) in enumerate(train_loader):
            images = images.to(device)
            labels = labels.to(device)

            # Forward -> Backward <- passes
            outputs = model(images)
            loss = train_criterion(outputs, labels)
            optimizer.zero_grad()  # zero out gradients for new minibatch
            loss.backward()

            optimizer.step()
            if (i_batch + 1) % 100 == 0:
                logging.info('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
                    epoch + 1, num_epochs, i_batch + 1, total_step, loss.item()))
        epoch_times.append(time.time() - epoch_start_time)
    train_time = time.time() - train_time
    logging.info("train time:{}".format(train_time))

    # Test the model
    logging.info('~+~' * 40)
    model.eval()
    test_time = time.time()
    test_score = eval(model, validation_loader, device)
    test_time = time.time() - test_time
    if save_model_str:
        # Save the model checkpoint can be restored via "model = torch.load(save_model_str)"
        if os.path.exists(save_model_str):
            save_model_str += '_'.join(time.ctime())
        torch.save(model.state_dict(), save_model_str)
    return test_score, train_time, test_time, total_model_params


def get_config_space():
    cs = CS.ConfigurationSpace()
    dataset = 1
    batch_size = CSH.UniformIntegerHyperparameter('batch_size', 100, 1500)
    learning_rate = CSH.UniformFloatHyperparameter('learning_rate', 0.0001, 0.01, log=True)
    layers = CSH.UniformIntegerHyperparameter('layers', 2, 8)
    kernel_size = CSH.CategoricalHyperparameter(name="kernel_size", choices=[3, 5])
    channels = CSH.UniformIntegerHyperparameter('channels', 2, 10)
    pooling = CSH.CategoricalHyperparameter('pooling', choices=[True, False])
    activation = CSH.CategoricalHyperparameter("activation", choices=["tanh", "sigmoid", "relu"])
    dropout = CSH.CategoricalHyperparameter("dropout", choices=[True, False])
    dropout_rate = CSH.UniformFloatHyperparameter("dropout_rate", 0.01, 0.3)


    # change number of channel based on depth
    change_channels = CSH.CategoricalHyperparameter(name="change_type", choices=["none", "linear"])
    channel_change_rate = CSH.UniformIntegerHyperparameter("change_rate", 1, 3)
    pool1 = CSH.CategoricalHyperparameter(name="PoolTypes1", choices=[0])
    pool2 = CSH.CategoricalHyperparameter(name="PoolTypes2", choices=[0,1])
    pool3 = CSH.CategoricalHyperparameter(name="PoolTypes3", choices=[0,1,2,3])

    optimizer = CSH.CategoricalHyperparameter(name="optimizer", choices=["adam", "adad", "sgd"])


    cs.add_hyperparameters([
        batch_size,
        learning_rate,
        layers,
        channels,
        pooling,
        change_channels,
        channel_change_rate,
        activation,
        pool1,
        pool2,
        pool3,
        optimizer,
        dropout,
        dropout_rate,
        kernel_size,
        ])

    pool_cond1 = CS.OrConjunction(CS.EqualsCondition(pool1, layers, 2),
                                  CS.EqualsCondition(pool1, layers, 3),
                                  CS.EqualsCondition(pool1, layers, 4),
                                  CS.EqualsCondition(pool1, layers, 5))

    pool_cond2 = CS.EqualsCondition(pool2, layers, 6)
    pool_cond3 = CS.OrConjunction(CS.EqualsCondition(pool3, layers, 7),
                                  CS.EqualsCondition(pool3, layers, 8))


    dropout_cond = CS.EqualsCondition(dropout_rate, dropout, True)


    cond_1 = CS.EqualsCondition(channel_change_rate, change_channels, "linear")
    cs.add_conditions([cond_1, pool_cond1, pool_cond2, pool_cond3, dropout_cond])
    return cs

def call_train(cfg, seed=41, instance=1):
    print(instance)
    for k in cfg:
        print(k)
        print(cfg[k])
    dimension_change = 0
    if cfg["change_type"] == "linear":
        dimension_change = cfg["change_rate"]

    if cfg["dropout"]:
        dropout = cfg["dropout_rate"]
    else:
        dropout = 0

    max_pooling = 0
    if cfg["layers"] < 6:
        max_pooling = 0
    elif cfg["layers"] == 6:
        max_pooling = cfg["PoolTypes2"]
    else:
        max_pooling = cfg["PoolTypes3"]
    test_score, train_time, test_time, total_model_params = train(
            train_dataset,
            test_dataset,
        {  # model architecture
            'n_layers': cfg["layers"],
            'n_conv_layer': cfg["layers"] - 1
        },
        num_epochs=1,
        batch_size=cfg["batch_size"],
        learning_rate=cfg["learning_rate"],
        activation=cfg["activation"],
        train_criterion=loss_dict["cross_entropy"],
        model_optimizer=opti_dict[cfg["optimizer"]],
        kernel_size=cfg["kernel_size"],
        channels=cfg["channels"],
        dropout=dropout,
        batchnorm=False,
        dimension_change=dimension_change,
        data_augmentations=None,
        pooling=max_pooling,
        save_model_str=None,
        valid_sampler=valid_sampler,
        train_sampler=train_sampler,
        ichannels=1,
        width=28,
        height=28
    )
    print(1 - test_score)
    return 1 - test_score


if __name__ == '__main__':
    loss_dict = {'cross_entropy': torch.nn.CrossEntropyLoss,
                 'mse': torch.nn.MSELoss}
    opti_dict = {'adam': torch.optim.Adam,
                 'adad': torch.optim.Adadelta,
                 'sgd': torch.optim.SGD}

    logging.basicConfig(level="INFO")
    # load the dataset
    dataset = 'K49'
    data_dir = "../data"
    data_augmentations = transforms.ToTensor()

    if dataset == 'KMNIST':
        train_dataset = KMNIST(data_dir, True, data_augmentations)
        test_dataset = KMNIST(data_dir, False, data_augmentations)
    elif dataset == 'K49':
        train_dataset = K49(data_dir, True, data_augmentations)
        test_dataset = K49(data_dir, False, data_augmentations)


    dataset = train_dataset
    validation_split = .2
    shuffle_dataset = True
    random_seed= 42
    
    # Creating data indices for training and validation splits:
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    split = int(np.floor(validation_split * dataset_size))
    if shuffle_dataset :
        np.random.seed(random_seed)
        np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    
    # Creating PT data samplers and loaders:
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    

    test_loader = DataLoader(dataset=test_dataset,
                             shuffle=False)



    cs = get_config_space()
    # Scenario object
    scenario = Scenario({"run_obj": "quality",   # we optimize quality (alternatively runtime)
                         "runcount-limit": 100,  # maximum function evaluations
                         "cs": cs,               # configuration space
                         "deterministic": "true",
                         "abort_on_first_run_crash": 'false',
                         "wallclock-limit": 14400,
                         "output_dir": "smac_run_1",
                         })

    print("Optimizing! Depending on your machine, this might take a few minutes.")

    smac = SMAC(scenario=scenario, rng=np.random.RandomState(42),
            tae_runner=call_train)
    
    incumbent = smac.optimize()

    # start the analyzing with cave
    #smac_out_dir = "./smac_run_1/"
    smac_out_dir = "./smac3-output_Run1/"
    runname = None
    for name in os.listdir(smac_out_dir):
        if name.startswith("run"):
            runname = name
            print(name)
            break

    print("\n\n\n\n")
    cave_in_folder = smac_out_dir + runname
    #cave = CAVE(folders=[cave_in_folder,], output_dir="cave_analysis_run1", ta_exec_dir=["smac_run_1"])
    cave = CAVE(folders=[cave_in_folder,], output_dir="cave_analysis_run1", ta_exec_dir=["smac3-output_Run1"])
    cave.analyze()
    important_params_fanova = list()
    if "fanova" in cave.param_imp:
        for p, v in cave.param_imp["fanova"].items():
            print(p, v)
            if v > 0.15:
                important_params_fanova.append(p)
    else:
        print("WARNING: No Fanova values....")

    print("\n\n\n\n")

    important_params_lpi = list()
    lpi = cave.local_parameter_importance()
    print("\n\n\n\n")
    print(lpi)
    if "lpi" in lpi.param_imp:
        for p, v in lpi.param_imp["lpi"].items():
            print(p, v)
            if v > 0.15:
                important_params_lpi.append(p)
    print("\n\n\n\n")

    important_params = set(important_params_fanova)
    for p in important_params_lpi:
        important_params.add(p)

    important_params = list(important_params)
    print(important_params)


    # read the json files
    with open (cave_in_folder + "/runhistory.json") as f:
        run_data = json.load(f)

    with open(cave_in_folder + "/configspace.json") as f:
        config_space = json.load(f)



    # find out what can be cancelled
    new_param_choices = dict()
    for param in important_params:
        new_param_choices[param] = None
        print("Investigating parameter:", param)
        # iterate over configspace to find parameter
        for hp in config_space["hyperparameters"]:
            if hp["name"] == param:
                if hp["type"] != "categorical":
                    print("To bad not categorical :(")
                    break
                print(hp["choices"])
                param_choices = hp["choices"]
                param_values = []
                for i in range(len(param_choices)):
                    param_values.append([])

                for run, config in run_data["configs"].items():
                    for i, choice in enumerate(param_choices):
                        if run_data["configs"][run][param] == choice:
                            param_values[i].append( run_data["data"][int(run)-1][1][0] )

                for i in range(len(param_values)):
                    param_values[i] = np.asarray(param_values[i])

                comparison_matrix = np.zeros((len(param_values), len(param_values)))
                for i in range(len(param_values) - 1):
                    for j in range(i + 1, len(param_values)):
                        s, p = stats.ttest_ind(param_values[i], param_values[j])
                        if p < 0.05:
                            if np.mean(param_values[i]) < np.mean(param_values[j]):
                                print("Choice {} is significantly better than choice {}".format(param_choices[i], param_choices[j]))
                                comparison_matrix[i, j] = 1
                                comparison_matrix[j, i] = -1
                            else:
                                print("Choice {} is significantly better than choice {}".format(param_choices[j], param_choices[i]))
                                comparison_matrix[i, j] = -1
                                comparison_matrix[j, i] = 1
                res = np.sum(comparison_matrix, axis=1)
                for i in range(len(res)):
                    if res[i] == len(res) -1:
                        new_param_choices[param] = [param_choices[i]]
                if not new_param_choices[param]:
                    new_param_choices[param] = list()
                    for i in range(len(res)):
                        if res[i] == - (len(res) - 1):
                            pass
                        else:
                            new_param_choices[param].append(param_choices[i])
                print("Hyperparameter", param, "can be reduced to:", new_param_choices[param])

    # open code and edit the config_space
    with open("smac_cnn_default_2.py") as f:
        code = f.readlines()

    for param in new_param_choices.keys():
        search_str = "CSH.CategoricalHyperparameter(name='{}'".format(param)
        for i in range(len(code)):
            if code[i].find(search_str) > 0:
                cut_off = code[i].find("choices=[")

                line = code[i][:cut_off] + "choices=["
                for o in new_param_choices[param]:
                    print(o)
                    line = line + '"' + o + '", '
                line = line + "])\n"
                print(line)
                code[i] = line
    
    with open("smac_next_iter.py", "w") as f:
        f.writelines(code)




                            







    



